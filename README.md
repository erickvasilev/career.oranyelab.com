# CAREER.ORANYELAB.COM

Career.Oranyelab.com is a simple application for Oranyelab Recruitment system. Based on:

  - Laravel 5.4
  - PHP 7
  - MariaDB

Connected to Mailgun & SMS-Notifikasi for notification automation.
  
### Installation

```sh
$ git clone https://gitlab.com/erickvasilev/career.oranyelab.com.git
```
Install the dependencies.

```sh
$ cd career.oranyelab.com
$ composer install
```

***

Edit **.env** file  and fill this line with your configuration:

DB_DATABASE=your_database_name

DB_USERNAME=your_database_user

DB_PASSWORD=your_user_password

MAILGUN_DOMAIN=yourdomain.com

MAILGUN_SECRET=Your_key-secret

***

Run Database Migration

```sh
$ php artisan migrate
```

***

Edit **\app\Http\Controllers\Auth\RegisterController.php** file:

	$userkey='abcdefgh';
	$passkey='hijklmn';
for connected to sms-notifikasi
***

**BUGS**
* "Position" field currently used for "Salary"

License
----
MIT

