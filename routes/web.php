<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/recruitment', function () {
    return view('recruitment');
});

Route::get('/position/backend', function () {
    return view('position.backend');
});

Route::get('/position/frontend', function () {
    return view('position.frontend');
});

Route::get('/position/app', function () {
    return view('position.app');
});

Route::get('/position/fullstack', function () {
    return view('position.fullstack');
});

Route::get('/position/animator', function () {
    return view('position.animator');
});

Route::get('/position/javascript', function () {
    return view('position.javascript');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/hello', 'HomeController@hello');
