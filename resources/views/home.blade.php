@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thank You</div>

                <div class="panel-body">
                    We have received your data, we will contact you if your skills fit our needs. Thanks.
					
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
