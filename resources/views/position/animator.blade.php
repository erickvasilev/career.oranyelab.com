@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>3D ANIMATOR</H3></div>
                <div class="panel-body">
                   <p>Oranyelab is a company engaged in Digital Mobile Advertising, today is growing rapidly
                   and currently require candidates who are professional and have high morale,	
				   to join our successful team, to fill 3D Animator position.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>Ability to create realistic 3D models, textures, illustrations, and animations</li>
					<li>Good sense with camera position and lighting</li>
					<li>Has a public portfolio of own animation work and assigned work</li>
                    <li>High competency with Blender is strong required</li>
					<li>A plus if you have experience with motion design and game engines such as Unity</li>
                    
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
