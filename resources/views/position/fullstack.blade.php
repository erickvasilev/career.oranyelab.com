@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>FULLSTACK DEVELOPER</H3></div>
                <div class="panel-body">
                   <p>Oranyelab is a company engaged in Digital Mobile Advertising, today is growing rapidly
                   and currently require candidates who are professional and have high morale,	
				   to join our successful team, to fill App Developer position.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>you have experience designing scalable REST APIs (Lumen) and deploying, monitoring, scaling and maintenance an application.</li>
					<li>Expert in using cloud service providers, like Amazon Web Services, Digital Ocean, or Google Cloud Engine</li>
					<li>Comprehensive experience supporting the MEAN stack ( MongoDB, Express.js, Angular, and Node.js)</li>
                    <li>Know how to handle and optimize high traffic server  (Load Balancing, Varnish, Redis, etc)</li>
                    
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
