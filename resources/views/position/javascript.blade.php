@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>JAVASCRIPT ENGINEER</H3></div>
                <div class="panel-body">
                   <p>We are looking to add a passionate and dedicated JavaScript Engineer to our ranks. 
				   You will be working on our application written in AngularJS that contains all the 
				   backoffices for all our products as a single unified SPA that will allow us to provide a single 
				   unified experience to our customers. </p><p>
				   We use a wide array of technologies and like to stay on the bleeding edge so you will be 
				   hacking on AngularJS, node.js for some CLI tools and backend services, CouchDB for component metadata storage, 
				   bower and npm (transitioning to just npm) for dependency management.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>Experience creating fast performing interfaces using ReactJS,jQuery and other JS frameworks</li>
					<li>Advanced understanding of JavaScript (ES6), including DOM manipulation and object model</li>
					<li>Experience with all stages of software development lifecycle</li>
                    <li>Familiar with version control and writing automated tests for the client side </li>
					
                    
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
