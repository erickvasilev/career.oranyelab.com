@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>APP DEVELOPER</H3></div>
                <div class="panel-body">
                   <p>Oranyelab is a company engaged in Digital Mobile Advertising, today is growing rapidly
                   and currently require candidates who are professional and have high morale,	
				   to join our successful team, to fill App Developer position.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>You have experience with Objective-C or Swift and Java</li>
					<li>Expert in app developement providers, such as: Firebase, Onesignal, Amazon S3, Fabric.io, Appstore, Playstore, and Auth0</li>
					<li>Comprehensive practical experience around XCode, Android Studio, and IntelXD. Also familiar with Ionic, Cordova, or other
						mobile framework</li>
                    <li>Deep understanding with React, Typescript, or Angular 2</li>
                    <li>Able to integrating App with JSON/XML Resful API</li>						
					 
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
