@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>FRONT-END PROGRAMMER</H3></div>
                <div class="panel-body">
                   <p>Oranyelab is a company engaged in Digital Mobile Advertising, today is growing rapidly
                   and currently require candidates who are professional and have high morale,	
				   to join our successful team, to fill Front-end programmer position.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>Skillfull to convert mockups (JPG/PSD/AI) into lightweight, clean and responsive html code</li>
					<li>Proven experience with client-side scripting and JavaScript/CSS libraries and frameworks, 
					    such as CSS3, Bootstrap/Materialize, jQuery, and Angular JS</li>
					<li>Proficient understanding of cross-browser compatibility and responsiveness issues and ways to work around them</li>
                    <li>Good understanding of asynchronous request handling, partial page updates, and AJAX</li>	
                   			
					 
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
