@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><H3>BACK-END PROGRAMMER</H3></div>
                <div class="panel-body">
                   <p>Oranyelab is a company engaged in Digital Mobile Advertising, today is growing rapidly
                   and currently require candidates who are professional and have high morale,	
				   to join our successful team, to fill Back-end programmer position.</p>
				   
				   <p><strong>Requirements</strong></p>
				   
				   <ol>
				    <li>Extensive knowledge and experience of MVC design pattern frameworks such
				         as: Laravel, Symfony, or CodeIgniter</li>
					<li>Familiar with Git for versioning control</li>
					<li>Strong skills in object-oriented web development with PHP, JavaScript, MySQL, CSS and HTML </li>
                    <li>Experienced in integrating REST API with Javascript / Angular</li>	
                    <li>Broad experience with NoSQL Databases (MongoDB, NeDB)</li>					
					 
					</ol>
					
					<p>For apply this position, simply <strong>register</strong> below.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
